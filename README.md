# Refclone

`refclone` is a wrapper for `git clone` command, that helps keeping cloned git
repos organized. 


### Installation

    pip3 install --user git+https://alcm-b@bitbucket.org/alcm-b/refclone.git


### Usage

    refclone.py --repohome=/home/username/ref https://gitserver.example.com/user/project

You'll probably want to keep the same `repohome` directory for all git projects
you clone. This can be achieved by creating this pair of aliases in
`~/.bashrc`:

    alias refclone='refclone --repohome=/home/username/ref'
    alias cdref='cd /home/username/ref'

After that, you can start using `refclone` like this:

    refclone https://gitserver.example.com/user/project
    cdref
    cd gitserver.example.com/user/project
    # start exploring the code


### Description

`refclone` ensures that all git repositories I clone are located in the same
place, nicely organized by server url, user and project name. It also shaves
off a couple of commands I have to run each time I want to take a look at the
source code of one more open source project.

Effectively, this utility is a shortcut for these two UNIX shell commands:

    mkdir -p "/<repohome>/<server-url>/<username>/<project-name>/"
    git clone <git-url> /<repohome>/<server-url/<username>/<project-name>/

, where `<repohome>` is provided in command line option `--repohome`. 
