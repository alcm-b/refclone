-include ~/bin/generic.mk

##
## Run tests
f2: testurl = https://alcm-b@bitbucket.org/alcm-b/refclone.git
f2:
	pyflakes ./refclone/ && py.test-3 --capture=no --verbose && \
	refclone $(testurl) --repohome=$$(mktemp -d /tmp/refclone/f2-XXX)
