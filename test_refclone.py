from unittest import mock
import pytest
import subprocess

import refclone


def test_get_clonedirectory():
    """Test various git URLs."""
    url = 'https://alcm-b@bitbucket.org/alcm-b/refclone.git'

    dd = refclone._get_clone_directory('/tmp', url)
    assert dd == '/tmp/bitbucket.org/alcm-b/refclone'


def test_nonexisting_clonedir():
    """Test for a directory that does not exist."""
    dd = refclone._get_clone_directory('/tmpzzz', 'anyurl')
    assert dd == '/tmpzzz/anyurl'


def test_clonedir_empty_name():
    """Test for a directory name that is an empty string."""
    dd = refclone._get_clone_directory('', 'anyurl')
    assert dd == 'anyurl'


@mock.patch('refclone.subprocess')
@mock.patch('refclone.os')
def test_clone(x_os, x_subprocess):
    """Test refclone.clone() happy path."""
    x_os.path.isdir.return_value = False
    x_subprocess.run.return_value = subprocess.CompletedProcess('', 0)

    orig_get_clone_directory = refclone._get_clone_directory
    with mock.patch('refclone._get_clone_directory') as x_get_clone_directory:
        x_get_clone_directory.return_value = '/tmp/git.example.com/user/repo'
        
        refclone.clone('git.example.com/user/repo', '/tmp/refdir')

        x_os.makedirs.assert_called_with('/tmp/git.example.com/user/repo')
        expected_cmd = 'git clone git.example.com/user/repo /tmp/git.example.com/user/repo'
        x_subprocess.run.assert_called_with(tuple(expected_cmd.split()))


@mock.patch('refclone.subprocess')
@mock.patch('refclone.os')
def test_clone_into_existing_dir(x_os, x_subprocess):
    """Test that clone aborts when directory already exists."""
    x_os.path.isdir.return_value = True

    with mock.patch('refclone._get_clone_directory') as x_get_clone_directory:
        x_get_clone_directory.return_value = '/tmp/existing_dir'
        refclone.clone('git.example.com/user/repo', '/tmp/refdir')
        x_os.path.makedirs.assert_not_called()
        x_subprocess.run.assert_not_called()


@mock.patch('refclone.subprocess')
@mock.patch('refclone.os')
def test_failed_git_clone_command(x_os, x_subprocess):
    """Test, what happens when git clone command fails."""
    x_os.path.isdir.return_value = False
    x_subprocess.run.return_value.returncode = 1

    with mock.patch('refclone._get_clone_directory') as x_get_clone_directory:
        x_get_clone_directory.return_value = '/tmp/git.example.com/user/repo'
        refclone.clone('git.example.com/user/repo', '/tmp/refdir')
        x_os.makedirs.assert_called_with('/tmp/git.example.com/user/repo')
        assert x_subprocess.run.called
